# MinecraftinatorV2

Converts .jpg and .png files into Minecraft pixel art.
Minecraft textures have to be provided by the user into the Textures folder.

To add textures:
1) Launch and close program
2) Drag all Minecraft textures to the newly generated ./Textures file

Usage:
The only paramater is "Block size in pixels". That parameter defines how many pixels will be replaced by a single Minecraft texture.

The lower the number, the higher the quality but higher resolution.
New image resolution is calculated by multiplying the source image resolution by 16 / "Block size in pixels".

E.G "Block size in pixels" = 10 means that the picture is divided into 10x10grids and each cell is replaced with a fitting Minecraft texture. This means that the created image will be 1.6 times larger.

Created images are placed into the ./Output folder.

Project was made in circa 2019. This is an old project that is just now backed up to Gitlab.
