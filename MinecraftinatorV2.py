from tkinter import *
from tkinter.filedialog import askopenfilename
from PIL import Image
import os
import math

dir_filename = ""

filetypes = [
    ("Image files", ".png"),
    ("Image files", ".jpg"),
]

bg_color = "#83684D"

def CheckTextures():
    try:
        os.chdir("./Textures")
        filenames = []

        #Get all .png files from the folder ./Textures, add them to list filenames
        for filename in os.listdir():
            if filename.endswith(".png"):
                filenames.append(filename)

        os.chdir("..")
        f = open("./Data.txt", "w")

        #Check if Data.txt is not empty
        if os.stat("./Data.txt").st_size != 0:
            #Start checking if textures in Data.txt match to textures in folder ./Textures
            for textline in f:
                try:
                    filenames.remove(textline[3])
                #Data.txt had an extra texture, redo scan
                except ValueError:
                    f.close()
                    filenames = []
                    ScanTextures()
                    break
            
            #./Textures had files, that weren't in the Data.txt, redo scan
            if filenames != []:
                f.close()
                ScanTextures()

        #Data.txt was empty, redo scan
        else:
            f.close()
            ScanTextures()

    except FileNotFoundError:
        os.mkdir("./Textures")
        ScanTextures()

def ScanTextures():
    """
    Scan .png files in the ./Textures folder, add their average RGB values and filenames to Data.txt
    """
    f = open("./Data.txt", "w")
    
    if len(os.listdir("./Textures")) == 0:
        f.close()
    
    else:
        os.chdir("./Textures")

        for filename in os.listdir():
            #Go through files in folder "./Textures"
            if filename.endswith(".png"):
                img = Image.open(filename)
                img = img.convert("RGB")
                width, height = img.size
                total_r = 0
                total_g = 0
                total_b = 0
                #Find average RGB value of image "filename"
                for x in range(width):
                    for y in range(height):
                        r, g, b = img.getpixel((x,y))
                        total_r += r
                        total_g += g
                        total_b += b
                average_r = int(total_r / (width * height))
                average_g = int(total_g / (width * height))
                average_b = int(total_b / (width * height))
                data_line = str(average_r) + " " + str(average_g)  + " " + str(average_b) + " " + filename
                #Save RGB value to ./Data.txt
                f.write(data_line)
                f.write("\n")
                
            else:
                continue

        os.chdir("..")
        f.close()

def distance (x1, y1, z1, x2, y2, z2):
    """
    Gets distance between 2 points in 3D, used for colors
    Treats RGB value as a point in 3D space
    """
    return math.sqrt( (x1 - x2)**2 + (y1 - y2)**2 + (z1 - z2)**2 )

def Choose_File():
    """
    Enables user to browse files.
    Updates file location, shows image size
    Function is activated when button "browse" is pressed.
    """
    global dir_filename
    dir_filename = askopenfilename(filetypes = filetypes)
    if dir_filename != "":

        #Show directory in GUI, check if dir is longer than 60 characters
        if len(dir_filename) <= 60:
            file_label.config(text=dir_filename)
        #Shorten directory to 60 units
        else:
            shortened_filename = [char for char in dir_filename]
            
            #Remove excess characters from beginning
            for i in range(len(dir_filename)-60):
                del shortened_filename[0]
            
            #Add dots to shortened directory
            for i in range(3):
                shortened_filename[i]="."
            
            #Recreate string from list
            shortened_filename = "".join(shortened_filename)
            file_label.config(text=shortened_filename)


        #Open image
        img = Image.open(dir_filename)

        #Get image resolution
        width, height = img.size

        #Show file resolution
        width_label.config(text="Width: " + str(width) )
        height_label.config(text="Height: " + str(height) )

        img.close()

    #Reset progressbar
    loading_bar_canvas.delete("all")

def Check_Pixels():
    #Check if pixel size is an integer above 0
    section_size = block_size.get()
    try:
        section_size = int(section_size)
        if section_size > 0:
            Create_Image(section_size)
        else:
            block_size.delete(0,"end")
    except ValueError:
        block_size.delete(0,"end")
        pass

def Create_Image(section_size):
    global dir_filename
    #Open file
    if file_label.cget("text") != "N/A":
        if len(os.listdir("./Textures")) != 0:
            file_label.config(text=dir_filename)
            img = Image.open(dir_filename)
        
            #Get image size
            width, height = img.size

            #Get filename from directory
            filename = dir_filename.split('/')[-1]

            #Strip file type from filename
            filename = filename.split(".")[0]

            #Calculate height and width in blocks
            block_width = math.floor(width / section_size)
            block_height = math.floor(height / section_size)

            #Create new image for output
            output = Image.new("RGB", (block_width*16, block_height*16), color="white")

            loading_bar_len = loading_bar_canvas.winfo_width()

            #Cycle through sections
            for x_section in range(1, block_width+1):
                #Calculate percentage
                progress = round(x_section / block_width, 3)

                #Find progress bar length
                progress_len = math.ceil(progress*loading_bar_len)
                #Update progress bar
                loading_bar_canvas.delete("all")
                loading_bar_canvas.create_rectangle(0,0,progress_len + 1,10, fill="blue", outline= "blue")
                loading_bar_canvas.update()

                for y_section in range(1, block_height+1):

                    #Zero the average rgb values
                    average_r = 0
                    average_g = 0
                    average_b = 0
                    n = 0
                    #Cycle through sections pixels, find average rgb value of the section
                    for x in range((x_section - 1) * section_size, x_section  * section_size):
                        for y in range((y_section - 1) * section_size, y_section * section_size):
                            #Since some images have an additional alpha value, add that to b if it exsists
                            r, g, *b = img.getpixel((x,y))
                            average_r += r
                            average_g += g
                            average_b += b[0]
                            n += 1

                    average_r = int(average_r / n)
                    average_g = int(average_g / n)
                    average_b = int(average_b / n)

                    #Find best block for the color
                    best_match = ""
                    best_distance = 500

                    with open("Data.txt", "r") as data:
                        while True:
                            string = data.readline().split()
                            if string == []:
                                break
                            test_distance = distance( average_r, average_g, average_b, int(string[0]), int(string[1]), int(string[2]) )
                            if test_distance < best_distance:
                                best_distance = test_distance
                                best_match = string[3]

                    image_location = "./Textures/" + best_match
                    block = Image.open(image_location).convert("RGB")
                    output.paste(block, ((x_section - 1) * 16, (y_section - 1) * 16))
            #Check if folder called Output exists, if not, create it
            if os.path.isdir("./Output") == False:
                os.mkdir("./Output")

            output.save("./Output/" + "Pixelated" + filename + ".png", "PNG")

            #Refresh progress bar
            loading_bar_canvas.delete("all")
            loading_bar_canvas.create_rectangle(0,0,loading_bar_len,10,fill="#3CB371", outline="#3CB371")

            data.close()
            img.close()
        
        else:
            #Edge case from beginning, notifies user that no ./Textures is empty
            file_label.config(text="Folder ./Textures is empty!")

#Setup main window
root = Tk()
root.title("Minecraftinator V2")

#Get screen resolution, according to Stack Overflow, doesn't work with multiple monitors, can't be bothered to check.
screen_width = int(root.winfo_screenwidth() * 0.30) #30% of screen width
screen_height = int(root.winfo_screenheight() * 0.15) #15% of screen height
screen_size = str(screen_width) + "x" + str(screen_height)
pad_x = int(screen_width*0.01)
pad_y = int(screen_height*0.01)


#Resize and decorate main window
root.geometry(screen_size)
root.resizable(width=False, height=False)
root.configure(background=bg_color)

#FRAME - Frame for buttons and loading bar
button_frame = Frame(root, bg = bg_color, width=screen_width-2*pad_x, height=int(screen_height*0.3))
button_frame.grid(column=0, row = 0, sticky = N+E+S+W, pady=pad_y, padx=pad_x)
button_frame.grid_propagate(False)

#WIDGET - Create Browse button
browse = Button(button_frame, text="Browse files", bg="#3CB371", command=Choose_File)
browse.grid(column=0, row=0, sticky=N+E+S+W)  

#WIDGET - Create image
build = Button(button_frame, text="Build", bg="#3CB371", command = Check_Pixels)
build.grid(column=1, row=0, sticky=N+S+E+W, padx=pad_x)

#Update canvas, to set loading bar height according to buttons
root.update()

#WIDGET - Loading bar
loading_bar_width = screen_width - build.winfo_width() - browse.winfo_width() - 4*pad_x
loading_bar_canvas= Canvas(button_frame, width=loading_bar_width, height=button_frame.winfo_height()/4)
loading_bar_canvas.grid(column=2, row=0, sticky=W+E)

#FRAME - Frame for showing directory [Some stackoverflow code sprinkled in :)]
dir_frame = Frame(root, bg="green", height=int(screen_height*0.33))
dir_frame.grid(column=0, row = 1, sticky = N+E+S+W, padx = pad_x)
dir_frame.grid_propagate(False)
dir_frame.columnconfigure(1, weight=10)
dir_frame.rowconfigure(0, weight=10)

#WIDGET - Create label "file:"
file_text_label = Label(dir_frame, anchor = W, text="File:")
file_text_label.grid(column=0, row=0, sticky=N+E+S+W)

#WIDGET - Create label for directory
file_label = Label(dir_frame, anchor = W, text="N/A")
file_label.grid(column=1, row = 0, sticky=W+E+N+S)

#FRAME - Frame for image resolution and pixels size
resolution_frame = Frame(root,background = bg_color, height=int(screen_height*0.3))
resolution_frame.grid(column=0, row = 2, sticky = N+E+S+W, pady = 4)
resolution_frame.grid_propagate(False)
for i in range(3):
    resolution_frame.columnconfigure(i, weight=10)
resolution_frame.rowconfigure(0, weight=10)

#WIDGET - Labels for image width and height
width_label = Label(resolution_frame, text="Width: N/A ")
width_label.grid(column=0, row=0, sticky = N+E+S+W, padx = 4)

height_label = Label(resolution_frame, text="Height: N/A ")
height_label.grid(column=1, row=0, sticky = N+E+S+W)

#WIDGET - Text for user "block size"
block_size_text = Label(resolution_frame, text="Block size in pixels:")
block_size_text.grid(column=2, row = 0, sticky = N+E+S+W, padx = 4)

#WIDGET - Entry for getting block size
block_size = Entry(resolution_frame, width = 4)
block_size.grid(column=3, row = 0, sticky = W, padx = 4, pady = 0)

#Finalize GUI, make sure measurments are set in stone
root.update()

CheckTextures()

mainloop()
